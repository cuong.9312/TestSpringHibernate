- add thư viện spring security vào pom.xml
- khai báo file configure spring-security:

Cách 1: thêm các dòng sau vào file web.xml
	<listener>
		<listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
	</listener>

	<context-param>
		<param-name>contextConfigLocation</param-name>
		<param-value>
			/WEB-INF/spring-security.xml,
			/WEB-INF/spring-database.xml
		</param-value>
	</context-param>
	
Cách 2: 
	thêm dòng sau vào web.xml 
	<listener>
		<listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
	</listener>
	
	và thêm vào applicationContext.xml các dòng khai báo:
	<import resource="spring-security.xml" />
	<import resource="spring-database.xml" />
	
(spring-security.xml và spring-database.xml là các file configure kết nối tới database và configure spring-security 
 như form login, url logout, có mã hóa mật khẩu không...)
 
 
 - Ví dụ form login:
 
  
			 <form name='loginForm' id="loginForm" class="col-md-6 "
					action="<c:url value='/j_spring_security_check' />" method='POST'>
					<div class="form-group">
						<label for="email" class="col-lg-4 control-label">Email: <span
							class="require">*</span>
						</label>
						<div class="col-lg-8">
							<p>
								<input type="text" placeholder="Email..."
									class="form-username form-control" id="email" name="email" />
							</p>
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="col-lg-4 control-label">Password: <span
							class="require">*</span>
						</label>
						<div class="col-lg-8">
							<p>
								<input type="password" placeholder="Password..."
									class="form-password form-control" id="password" name="password" />
							</p>
						</div>
					</div>
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
			
			
					<div class="row">
						<label></label>
						<div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">
							<c:if test="${param.error != null}">
								<p style="color: red">Invalid username and password.</p>
							</c:if>
							<c:if test="${param.logout != null}">
								<p style="color: blue;">You have been logged out.</p>
							</c:if>
						</div>
					</div>
					<div class="row">
						<label></label>
						<div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">
							<button type="submit" class="btn btn-primary">Log in</button>
						</div>
					</div>
					<div class="row">
						<div
							class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-10 padding-right-30">
							<hr>
			
						</div>
					</div>
			</form>


- Ví dụ form logout:

			<c:url value="/j_spring_security_logout" var="logoutUrl" />
			<form action="${logoutUrl}" method="post" id="logoutForm">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>
			<script>
				function formSubmit() {
					document.getElementById("logoutForm").submit();
				}
			</script>
			
			<security:authorize access="isAuthenticated()">
				<security:authorize access="hasAnyRole('ROLE_ADMIN')">
				<a href="javascript:formSubmit()"> Logout</a>
			</security:authorize>
			
			
- mã hóa mật khẩu với spring-security:

		+ thực hiện mã hóa mật khẩu trước khi insert vào database, ví du:
		BCryptPasswordEncoder encrypt = new BCryptPasswordEncoder();
			user.setPassword(encrypt.encode(user.getPassword()));
			
		+ thực hiện hash password khi login bằng cách:
		thêm 			<password-encoder hash="bcrypt" /> vào spring-security.xml
		
			<!-- Select users and user_roles from database -->
			<authentication-manager>
				<authentication-provider>
					<password-encoder hash="bcrypt" />
					<jdbc-user-service data-source-ref="dataSource"
						users-by-username-query="select email,password, enabled from users where email=?"
						authorities-by-username-query="select users.email, role.name from users, user_roles, role
						where users.id=user_roles.users and role.id= user_roles.role and email =?  " />
				</authentication-provider>
			</authentication-manager>
		