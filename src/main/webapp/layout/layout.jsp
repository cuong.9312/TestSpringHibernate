<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html>
<html>
<head>
<!-- Basic Page Needs
    ================================================== -->
<meta charset="utf-8">
<title><tiles:getAsString name="title" /></title>

<link href="<c:url value="/assets/bootstrap/css/bootstrap.min.css" />"
	rel="stylesheet">
<script src="<c:url value="/assets/jquery/jquery.js" />"></script>
<script src="<c:url value="/assets/bootstrap/js/bootstrap.min.js" />"></script>
</head>
<script src="<c:url value="/assets/jquery/jquery.validate.min1.9.js" />"></script>
<link href="<c:url value="/assets/css/font-awesome.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/assets/css/owl.theme.css" />"
	rel="stylesheet">
<link href="<c:url value="/assets/css/red.css" />" rel="stylesheet">
<link href="<c:url value="/assets/css/custom.css" />" rel="stylesheet">

</head>

<body>

	<tiles:insertAttribute name="header" />

	<tiles:insertAttribute name="body" />

	<section id="footer" style="margin-top: 50px">
		<div class="footer_b">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="footer_bottom">
							<p class="text-block">
								&copy; Copyright reserved to <span>Test Spring +
									Hibernate </span>
							</p>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="footer_mid pull-right">
							<ul class="social-contact list-inline">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-rss"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i> </a></li>
								<li><a href="#"> <i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"> <i class="fa fa-pinterest"></i></a></li>
							</ul>
						</div>
					</div>

				</div>
			</div>
		</div>
	</section>
	<!-- Footer Area End -->



</body>
</html>
