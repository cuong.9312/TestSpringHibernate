<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:getAsString name="title" /></title>
<!-- <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<script src="assets/jquery/jquery.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/jquery/jquery.validate.min1.9.js"></script>
 -->
<link href="<c:url value="/assets/bootstrap/css/bootstrap.min.css" />"
	rel="stylesheet">
<script src="<c:url value="/assets/jquery/jquery.js" />"></script>
<script src="<c:url value="/assets/bootstrap/js/bootstrap.min.js" />"></script>
</head>
<script src="<c:url value="/assets/jquery/jquery.validate.min1.9.js" />"></script>
<body>

	<div class="container" style="margin-top: 20px">

		<div class="row col-md-12 ">
			<!-- BEGIN HEADER -->
			<tiles:insertAttribute name="header" />
			<!-- END HEADER -->
		</div>

		<div class="row col-md-12 ">
			<!-- BEGIN BODY-->
			<tiles:insertAttribute name="body" />
			<!-- END BODY-->
		</div>


	</div>

</body>


</html>