<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras"
	prefix="tilesx"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
<tilesx:useAttribute name="pagecurrent" />



<!-- BEGIN FORM SUBMIT SPRING SECURITY LOGOUT -->
<c:url value="/j_spring_security_logout" var="logoutUrl" />
<form action="${logoutUrl}" method="post" id="logoutForm">
	<input type="hidden" name="${_csrf.parameterName}"
		value="${_csrf.token}" />
</form>
<script>
	function formSubmit() {
		document.getElementById("logoutForm").submit();
	}
</script>
<!-- END FORM SUBMIT SPRING SECURITY LOGOUT -->

<header id="section_header" class="navbar-fixed-top main-nav"
	role="banner">
	<div class="container">
		<!-- <div class="row"> -->
		<div class="navbar-header ">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#"> <img
				src="<c:url value="/assets/images/logo.png" />" alt="">
			</a>
		</div>
		<!--Navbar header End-->
		<nav class="collapse navbar-collapse navigation"
			id="bs-example-navbar-collapse-1" role="navigation">
			<ul class="nav navbar-nav navbar-right ">
				<li class="${pagecurrent=='home'?'active':'' }"><a
					href="<c:url value="/spring/index"/>">Home</a></li>
				<security:authorize access="!isAuthenticated()">
					<li class="${pagecurrent=='login'?'active':'' }"><a
						href="<c:url value="/spring/login"/>">Login</a></li>
					<li class="${pagecurrent=='register'?'active':'' }"><a
						href="<c:url value="/spring/register"/>">Register</a></li>
				</security:authorize>
				<security:authorize access="isAuthenticated()">
					<li class="${pagecurrent=='user'?'active':'' }"><a
						href="<c:url value="/spring/info"/>">Users</a></li>
					<security:authorize access="hasAnyRole('ROLE_ADMIN')">
						<li class="${pagecurrent=='listUser'?'active':'' }"><a
							href="<c:url value="/spring/user/listUser"/>">List user</a></li>
					</security:authorize>
					<li><a href="javascript:formSubmit()"> Logout</a></li>
				</security:authorize>
			</ul>
		</nav>
	</div>
	<!-- /.container-fluid -->
</header>

<%-- 
<!-- Static navbar -->
<nav class="navbar navbar-default">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed"
			data-toggle="collapse" data-target="#navbar" aria-expanded="false"
			aria-controls="navbar">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="#">Project name </a>
	</div>
	<div id="navbar" class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
			<li class="${pagecurrent=='home'?'active':'' }"><a
				href="<c:url value="/spring/index"/>">Home</a></li>
			<security:authorize access="!isAuthenticated()">
				<li class="${pagecurrent=='login'?'active':'' }"><a
					href="<c:url value="/spring/login"/>">Login</a></li>
				<li class="${pagecurrent=='register'?'active':'' }"><a
					href="<c:url value="/spring/register"/>">Register</a></li>
			</security:authorize>
			<security:authorize access="isAuthenticated()">
				<li class="${pagecurrent=='user'?'active':'' }"><a
					href="<c:url value="/spring/user/detailUser"/>">Users</a></li>
				<security:authorize access="hasAnyRole('ROLE_ADMIN')">
					<li class="${pagecurrent=='listUser'?'active':'' }"><a
						href="<c:url value="/spring/user/listUser"/>">List user</a></li>
				</security:authorize>
				<li><a href="javascript:formSubmit()"> Logout</a></li>
			</security:authorize>

		</ul>

	</div>
	<!--/.nav-collapse -->
</nav>

 --%>