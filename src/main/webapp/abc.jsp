<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>abc</title>
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<script src="assets/jquery/jquery.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/jquery/jquery.validate.min1.9.js"></script>

</head>
<body>

	<div class="container" style="margin-top: 20px">


		<!-- Static navbar -->
		<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Project name</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#">Home</a></li>
					<li><a href="#">About</a></li>
					<li><a href="#">Contact</a></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">Dropdown <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else here</a></li>
							<li role="separator" class="divider"></li>
							<li class="dropdown-header">Nav header</li>
							<li><a href="#">Separated link</a></li>
							<li><a href="#">One more separated link</a></li>
						</ul></li>
				</ul>

			</div>
			<!--/.nav-collapse -->
		</div>
		<!--/.container-fluid --> </nav>

		<div class="col-md-12 "
			style="background-color: #e6e6e6; padding-bottom: 50px;">

			<form class="form-horizontal form-without-legend" id="register-form"
				style="margin-top: 50px">
				<div class="form-group">
					<label for="email" class="col-lg-4 control-label">Email <span
						class="require">*</span>
					</label>
					<div class="col-lg-8">
						<input type="text" class="form-control" id="email" name="email">
					</div>
				</div>
				<div class="form-group">
					<label for="email" class="col-lg-4 control-label">Nick
						name: <span class="require">*</span>
					</label>
					<div class="col-lg-8">
						<input type="text" class="form-control" id="nickName"
							name="nickName">
					</div>
				</div>
				<div class="form-group">
					<label for="password" class="col-lg-4 control-label">Password:
						<span class="require">*</span>
					</label>
					<div class="col-lg-8">
						<input type="password" class="form-control" id="password"
							name="password">
					</div>
				</div>
				<div class="form-group">
					<label for="password" class="col-lg-4 control-label">Retype
						password: <span class="require">*</span>
					</label>
					<div class="col-lg-8">
						<input type="password" class="form-control" id="password_again"
							name="password_again">
					</div>
				</div>
				<div class="row">
					<div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">
						<button type="submit" class="btn btn-primary">Register</button>
					</div>
				</div>
				<div class="row">
					<div
						class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-10 padding-right-30">
						<hr>

					</div>
				</div>
			</form>
		</div>

	</div>
	<!-- /container -->


</body>
<script>
	// When the browser is ready...
	$(function() {

		// Setup form validation on the #register-form element
		$("#register-form")
				.validate(
						{
							// Specify the validation rules
							rules : {
								nickName : "required",
								email : {
									required : true,
									email : true
								},
								password : {
									required : true,
									minlength : 5
								},
								password_again : {
									required : true,
									minlength : 5,
									equalTo : "#password"
								},
							},

							// Specify the validation error messages
							messages : {
								nickName : "Please enter your nick name",
								password : {
									required : "Please provide a password",
									minlength : "Your password must be at least 5 characters long"
								},
								email : "Please enter a valid email address",
							},

							submitHandler : function(form) {
								form.submit();
							}
						});

	});
</script>


</html>