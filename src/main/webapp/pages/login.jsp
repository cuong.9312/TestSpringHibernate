<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<section id="contact">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="feature_header text-center">
					<h4 class="feature_sub">Welcome to Java Exercise.</h4>
					<div class="divider"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="contact_full">

				<form name='loginForm' id="loginForm" class="col-md-6 "
					action="<c:url value='/j_spring_security_check' />" method='POST'>
					<div class="form-group">
						<label for="email" class="col-lg-4 control-label">Email: <span
							class="require">*</span>
						</label>
						<div class="col-lg-8">
							<p>
								<input type="text" placeholder="Email..."
									class="form-username form-control" id="email" name="email" />
							</p>
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="col-lg-4 control-label">Password:
							<span class="require">*</span>
						</label>
						<div class="col-lg-8">
							<p>
								<input type="password" placeholder="Password..."
									class="form-password form-control" id="password"
									name="password" />
							</p>
						</div>
					</div>
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />


					<div class="row">
						<label></label>
						<div
							class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">
							<c:if test="${param.error != null}">
								<p style="color: red">Invalid username and password.</p>
							</c:if>
							<c:if test="${param.logout != null}">
								<p style="color: blue;">You have been logged out.</p>
							</c:if>
						</div>
					</div>
					<div class="row">
						<label></label>
						<div
							class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">
							<button type="submit" class="btn btn-primary">Log in</button>
						</div>
					</div>
					<div class="row">
						<div
							class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-10 padding-right-30">
							<hr>

						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>











<script>
	// When the browser is ready...
	$(function() {

		// Setup form validation on the #register-form element
		$("#loginForm").validate({
			errorClass : "my-error-class",
			// Specify the validation rules
			rules : {
				email : {
					required : true,
					email : true
				},
				password : {
					required : true,
				},
			},

			// Specify the validation error messages
			messages : {
				password : {
					required : "Please provide a password",
				},
				email : "Please enter a valid email address",
			},

			submitHandler : function(form) {
				form.submit();

			}
		});

	});
</script>



