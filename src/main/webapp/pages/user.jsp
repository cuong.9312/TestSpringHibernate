<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
.my-error-class {
	color: red;
	text-align: left;
}
</style>





<section id="contact">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="feature_header text-center">
					<h4 class="feature_sub">Your Account.</h4>
					<div class="divider"></div>
					<c:if test="${param.Info == '0'}">
						<div class="alert alert-warning col-md-12">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong>Failed!</strong> Can not save your infomation!
						</div>

					</c:if>

					<c:if test="${param.Info == '1'}">
						<div class="alert alert-success col-md-12">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong>Success!</strong> save your infomation success!
						</div>

					</c:if>
					<c:if test="${param.pass == '0'}">
						<div class="alert alert-warning col-md-12">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong>Failed!</strong> Can not change your password!
						</div>

					</c:if>
					<c:if test="${param.pass == '3'}">
						<div class="alert alert-warning col-md-12">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong>Failed!</strong> Your old password is wrong!
						</div>

					</c:if>

					<c:if test="${param.pass== '1'}">
						<div class="alert alert-success col-md-12">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong>Success!</strong> Change password success!
						</div>

					</c:if>

				</div>
			</div>
		</div>
		<div class="row">
			<div class="contact_full">


				<form class="form-horizontal form-without-legend " id="form1"
					style="margin-top: 50px"
					action="<c:url value="/spring/user/editInfo"/>">

					<table class="table table-bordered table-hover">

						<tr>
							<td class="col-md-5">Your Email:</td>
							<td><input disabled="disabled" type="text"
								class="form-control" value="${user.email }"></td>
						</tr>
						<tr>
							<td>Your name:</td>
							<td><input type="text" class="form-control" id="name"
								name="name" value="${user.name}"></td>
						</tr>
						<tr>
							<td>Your phone number:</td>
							<td><input type="text" class="form-control" id="phoneNumber"
								name="phoneNumber" value="${user.phoneNumber }"></td>
						</tr>
						<tr>
							<td></td>
							<td><button type="submit" class="btn btn-primary">Save</button></td>
						</tr>
					</table>

				</form>


				<c:if test="${user.isAdmin == 'TRUE'}">
					<form class="form-horizontal form-without-legend " id="form2"
						style="margin-top: 50px"
						action="<c:url value="/spring/user/editPass"/>">

						<table class="table table-bordered ">

							<tr>
								<td class="col-md-5">Change your password:</td>
								<td><input type="password" class="form-control"
									id="old_password" name="old_password"
									placeholder="Enter old password" /></td>
							</tr>
							<tr>
								<td></td>
								<td><input type="password" class="form-control"
									id="password" name="password" placeholder="Enter new password" /></td>
							</tr>
							<tr>
								<td></td>
								<td><input type="password" class="form-control"
									id="password_again" name="password_again"
									placeholder="Retype new password" /></td>
							</tr>
							<tr>
								<td></td>
								<td><button type="submit" class="btn btn-primary">Save</button></td>
							</tr>
						</table>

					</form>
				</c:if>




			</div>
		</div>
	</div>
</section>




<script>
	// When the browser is ready...
	$(function() {
		$.validator.addMethod("validate_phone", function(value, element) {
			return this.optional(element) || /[0-9].{10,12}/.test(value);
		}, "Please enter a valid phone number.");
		$.validator
				.addMethod(
						"validate_password",
						function(value, element) {
							return this.optional(element)
									|| /((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})/
											.test(value);
						},
						"Passwords:<br>- must contains one digit from 0-9<br>- must contains one lowercase characters <br>- must contains one uppercase characters <br>- length at least 6 characters and maximum of 20	");

		$("#form1").validate({

			errorClass : "my-error-class",
			// Specify the validation rules
			rules : {
				name : "required",
				phoneNumber : {
					validate_phone : true,
					required : true
				},
			},

			// Specify the validation error messages
			messages : {
				name : "Please enter your name",
			},

			submitHandler : function(form) {
				form.submit();
			}
		});
		$("#form2")
				.validate(
						{

							errorClass : "my-error-class",
							// Specify the validation rules
							rules : {
								password : {
									required : true,
									validate_password : true,
									minlength : 8
								},
								password_again : {
									required : true,
									equalTo : "#password"
								},
							},

							// Specify the validation error messages
							messages : {
								password : {
									required : "Please provide a password",
									minlength : "Your password must be at least 6 characters long"
								},
							},

							submitHandler : function(form) {
								form.submit();
							}
						});

	});
</script>
