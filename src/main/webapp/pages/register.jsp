<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
.my-error-class {
	color: red;
}
</style>


<section id="contact">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="feature_header text-center">
					<h4 class="feature_sub">Create new account.</h4>
					<div class="divider"></div>
					<c:if test="${param.ok == '0'}">
						<div class="alert alert-warning col-md-12">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong>Failed!</strong> Can not register!
						</div>

					</c:if>
					<c:if test="${param.ok == '3'}">
						<div class="alert alert-warning col-md-12">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong>Failed!</strong> this email address has already been
							registered!
						</div>

					</c:if>

					<c:if test="${param.ok== '1'}">
						<div class="alert alert-success col-md-12">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong>Success!</strong> Register success!
						</div>

					</c:if>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="contact_full">

				<form class="form-horizontal form-without-legend col-md-8"
					id="register-form" style="margin-top: 50px"
					action="<c:url value="/spring/doRegister"/>">
					<div class="form-group">
						<label for="email" class="col-lg-4 control-label">Email <span
							class="require">*</span>
						</label>
						<div class="col-lg-8">
							<input type="text" class="form-control" id="email" name="email">
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="col-lg-4 control-label">Name: <span
							class="require">*</span>
						</label>
						<div class="col-lg-8">
							<input type="text" class="form-control" id="name" name="name">
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="col-lg-4 control-label">Phone
							number: <span class="require">*</span>
						</label>
						<div class="col-lg-8">
							<input type="text" class="form-control" id="phoneNumber"
								name="phoneNumber">
						</div>
					</div>
					<div class="form-group">
						<label for="password" class="col-lg-4 control-label">Password:
							<span class="require">*</span>
						</label>
						<div class="col-lg-8">
							<input type="password" class="form-control" id="password"
								name="password">
						</div>
					</div>
					<div class="form-group">
						<label for="password" class="col-lg-4 control-label">Retype
							password: <span class="require">*</span>
						</label>
						<div class="col-lg-8">
							<input type="password" class="form-control" id="password_again"
								name="password_again">
						</div>
					</div>
					<div class="row">
						<div
							class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">
							<button type="submit" class="btn btn-primary">Register</button>
						</div>
					</div>
					<div class="row">
						<div
							class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-10 padding-right-30">
							<hr>

						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<script>
	// When the browser is ready...
	$(function() {
		$.validator.addMethod("validate_phone", function(value, element) {
			return this.optional(element) || /[0-9]{10,12}/.test(value);
		}, "Please enter a valid phone number.");
		$.validator
				.addMethod(
						"validate_password",
						function(value, element) {
							return this.optional(element)
									|| /((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})/
											.test(value);
						},
						"Passwords:<br>- must contains one digit from 0-9<br>- must contains one lowercase characters <br>- must contains one uppercase characters <br>- length at least 6 characters and maximum of 20	");

		// Setup form validation on the #register-form element
		$("#register-form")
				.validate(
						{

							errorClass : "my-error-class",
							// Specify the validation rules
							rules : {
								name : "required",
								email : {
									required : true,
									email : true
								},
								phoneNumber : {
									validate_phone : true,
									required : true
								},
								password : {
									required : true,
									validate_password : true,
									minlength : 8
								},
								password_again : {
									required : true,
									equalTo : "#password"
								},
							},

							// Specify the validation error messages
							messages : {
								name : "Please enter your name",
								password : {
									required : "Please provide a password",
									minlength : "Your password must be at least 6 characters long"
								},
								email : "Please enter a valid email address",
							},

							submitHandler : function(form) {
								form.submit();
							}
						});

	});
</script>