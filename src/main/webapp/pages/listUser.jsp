<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>



<section id="contact">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="feature_header text-center">
					<h4 class="feature_sub">Manage account.</h4>
					<div class="divider"></div>
					<c:if test="${param.del == '0'}">
	<div class="alert alert-warning col-md-12">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Failed!</strong> Can not delete this user!
	</div>

</c:if>

<c:if test="${param.del == '1'}">
	<div class="alert alert-success col-md-12">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Success!</strong> Delete success!
	</div>

</c:if>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="contact_full">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>STT</th>
							<th>Email</th>
							<th>Name</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>

						<c:forEach items="${users}" var="user" varStatus="loop">
							<tr>
								<td>${loop.index}</td>
								<td>${user.email}</td>
								<td>${user.name}</td>
								<td class="col-md-2"><a type="button" data-toggle="modal"
									data-target="#myModal_${loop.index }"
									class="btn btn-default btn-danger ${user.isAdmin == 'TRUE'?'disabled':'' }">
										Delete </a> <a type="button" class="btn btn-default btn-info"
									data-toggle="modal" data-target="#viewModal_${loop.index }">
										View </a></td>
							</tr>


							<c:if test="${user.isAdmin != 'TRUE'}">
								<!-- Modal -->
								<div class="modal fade" id="myModal_${loop.index }"
									role="dialog">
									<div class="modal-dialog">

										<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												Are you sure you want to delete "${user.name}"?
											</div>
											<div class="modal-body" style="text-align: right;">
												<a type="button" class="btn btn-default btn-danger"
													href="<c:url value="/spring/user/delUser/${user.id} "/>">
													Delete </a> <a type="button" class="btn btn-default"
													data-dismiss="modal"> Cancel </a>
											</div>
										</div>

									</div>
								</div>
							</c:if>



							<div class="modal fade" id="viewModal_${loop.index }"
								role="dialog">
								<div class="modal-dialog">

									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4>Detail: "${user.name}"?</h4>
										</div>
										<div class="modal-body" style="text-align: left;">
											<b>Email:</b> ${user.email }<br> <b> Name:</b>
											${user.name }<br> <b>Phone number:</b>
											${user.phoneNumber}<br>

										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default"
												data-dismiss="modal">Close</button>
										</div>
									</div>

								</div>
							</div>
						</c:forEach>



					</tbody>
				</table>

			</div>
		</div>
	</div>
</section>
