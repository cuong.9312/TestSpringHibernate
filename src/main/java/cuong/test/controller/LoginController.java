package cuong.test.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import cuong.test.dao.UserDAO;
import cuong.test.entities.User;
import cuong.test.service.UserService;
import cuong.test.service.ValidatorService;

@Controller
public class LoginController {
	@RequestMapping("/login")
	public String login() {
		return "login";
	}

	/*
	 * @RequestMapping(value = "/doLogin", method = RequestMethod.POST) public
	 * String doLogin(HttpServletRequest request, HttpSession session, ModelMap
	 * model) { String email = request.getParameter("email"); String password =
	 * request.getParameter("password"); System.out.println(email);
	 * System.out.println(password); User user = UserDAO.checkLogin(email,
	 * password); if (user != null) { session.setAttribute("user", user); return
	 * "home"; } else { model.addAttribute("ok", "0"); return "redirect:login";
	 * }
	 * 
	 * }
	 */

	@RequestMapping("/register")
	public String register() {
		return "register";
	}

	@RequestMapping("/doRegister")
	public String doRegister(HttpServletRequest request, ModelMap model) {
		String email = request.getParameter("email");
		if (UserService.isExist(email)) {
			return "redirect:register?ok=3";
		}
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String phoneNumber = request.getParameter("phoneNumber");
		Date dateCreated = new Date();
		User user = new User(email, password, name, dateCreated, phoneNumber);
		System.out.println(user);
		if (UserDAO.insert(user) && ValidatorService.checkEmail(email)
				&& ValidatorService.checkPassword(password)) {
			return "redirect:register?ok=1";

		} else {
			return "redirect:register?ok=0";
		}

	}

	/*
	 * @RequestMapping("/doLogout") public String doLogout(HttpSession session)
	 * { session.removeAttribute("user"); return "redirect:login?ok=1"; }
	 */
}
