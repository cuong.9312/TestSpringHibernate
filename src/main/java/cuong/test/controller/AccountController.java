package cuong.test.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import cuong.test.entities.User;
import cuong.test.entities.wrapper.UserWrapper;
import cuong.test.service.UserService;
import cuong.test.service.ValidatorService;

@Controller
@RequestMapping("/user")
public class AccountController {
	@RequestMapping("/detailUser")
	public String detailUser(ModelMap model) {
		return "user";
	}

	@RequestMapping("/listUser")
	public String listUser(ModelMap model) {
		List<User> users1 = UserService.getAll();
		List<UserWrapper> users = new ArrayList<UserWrapper>();
		for (User user : users1) {
			UserWrapper userWrapper = UserService.convert(user);
			users.add(userWrapper);
		}

		model.addAttribute("users", users);
		return "listUser";
	}

	@RequestMapping("/delUser/{id}")
	public String delUser(@PathVariable int id) {
		if (UserService.delete(id)) {
			return "redirect:/spring/user/listUser?del=1";
		} else {
			return "redirect:/spring/user/listUser?del=0";
		}
	}

	@RequestMapping("/editInfo")
	public String editInfo(HttpSession session, HttpServletRequest request) {
		String name = (String) request.getParameter("name");
		String phoneNumber = (String) request.getParameter("phoneNumber")
				.toString();
		User user = (User) session.getAttribute("user");
		user.setPhoneNumber(phoneNumber);
		user.setName(name);
		if (UserService.updateInfo(user)
				&& ValidatorService.checkPhoneNumber(phoneNumber)) {
			return "redirect:/spring/user/detailUser?Info=1";
		} else {
			return "redirect:/spring/user/detailUser?Info=0";
		}
	}

	@RequestMapping("/editPass")
	public String editPass(HttpSession session, HttpServletRequest request) {
		String password = (String) request.getParameter("password");
		String oldPassword = (String) request.getParameter("old_password");

		BCryptPasswordEncoder encrypt = new BCryptPasswordEncoder();
		oldPassword = encrypt.encode(oldPassword);

		User user = (User) session.getAttribute("user");

		if (!user.getPassword().equals(oldPassword)) {
			return "redirect:/spring/user/detailUser?pass=3";

		}

		user.setPassword(password);
		if (UserService.update(user)) {
			return "redirect:/spring/user/detailUser?pass=1";
		} else {
			return "redirect:/spring/user/detailUser?pass=0";
		}
	}
}
