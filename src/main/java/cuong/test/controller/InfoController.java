package cuong.test.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cuong.test.entities.User;
import cuong.test.entities.wrapper.UserWrapper;
import cuong.test.service.UserService;

@Controller
public class InfoController {
	@RequestMapping("/info")
	public String showInfo(HttpSession session) {
		User user = (User) session.getAttribute("user");
		UserWrapper userWrapper = UserService.convert(user);
		session.setAttribute("user", userWrapper);
		return "user";
	}
}
