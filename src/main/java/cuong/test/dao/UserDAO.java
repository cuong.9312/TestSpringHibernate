package cuong.test.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import cuong.test.entities.Role;
import cuong.test.entities.User;

public class UserDAO {
	static SessionFactory sessionFactory = HibernateUtils.getSessionFactory();

	public static User checkLogin(String email, String password) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		User user;
		try {
			List<User> list = session
					.createQuery(
							"from User where email = :email and password= :password")
					.setParameter("email", email)
					.setParameter("password", password).list();
			if (list.size() != 0) {
				user = list.get(0);
				return user;
			} else {
				return null;
			}
		} catch (Exception ex) {
			session.getTransaction().rollback();
			ex.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public static boolean insert(User user) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		try {
			BCryptPasswordEncoder encrypt = new BCryptPasswordEncoder();
			user.setPassword(encrypt.encode(user.getPassword()));
			Role role = RoleDAO.getById(2);
			user.getRoles().add(role);
			session.save(user);
			session.getTransaction().commit();
			System.out.println("insert success!");
			return true;
		} catch (RuntimeException e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
	}

	public static boolean update(User user) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		try {
			BCryptPasswordEncoder encrypt = new BCryptPasswordEncoder();
			user.setPassword(encrypt.encode(user.getPassword()));
			
			
			
			session.saveOrUpdate(user);
			session.getTransaction().commit();
			System.out.println("update success!");
			return true;
		} catch (RuntimeException e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		
	}

	public static boolean updateInfo(User user) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(user);
			session.getTransaction().commit();
			System.out.println("update success!");
			return true;
		} catch (RuntimeException e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
	}

	public static boolean delete(User user) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		try {
			session.delete(user);
			session.getTransaction().commit();
			System.out.println("delete success!");
			return true;
		} catch (RuntimeException e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
	}

	public static boolean delete(Integer id) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		try {
			User user = session.get(User.class, id);
			session.delete(user);
			session.getTransaction().commit();
			System.out.println("delete success!");
			return true;
		} catch (RuntimeException e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
	}

	public static List<User> getAll() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		try {
			List<User> list = session.createQuery("from User").list();
			return list;
		} catch (RuntimeException e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public static User getById(int id) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		try {
			User user = session.get(User.class, id);
			return user;
		} catch (RuntimeException e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public static void showAll() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		try {
			List<User> list = session.createQuery("from User").list();
			for (User test : list) {
				System.out.println(test);
			}
		} catch (RuntimeException e) {
			session.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public static User getByEmail(String email) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		try {
			List<User> list = session
					.createQuery("from User where email = :email")
					.setParameter("email", email).list();
			if (list != null) {
				return list.get(0);
			} else {
				return null;
			}
		} catch (RuntimeException e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public static boolean isExist(String email) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		try {
			List<User> list = session
					.createQuery("from User where email = :email")
					.setParameter("email", email).list();
			return list.size() != 0;
		} catch (RuntimeException e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
	}

}
