package cuong.test.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import cuong.test.entities.Role;

public class RoleDAO {
	static SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

	public static Role intsert(Role role) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		try {
			session.save(role);
			session.getTransaction().commit();
			System.out.println("insert role success");
			return role;
		} catch (RuntimeException e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			return null; 
		} finally {
			session.close();
		}
	}

	public static Role saveOrUpdate(Role role) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		try {
			session.update(role);
			session.getTransaction().commit();
			System.out.println("update role success");
			return role;
		} catch (RuntimeException e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public static boolean delete(Role role) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		try {
			session.delete(role);
			session.getTransaction().commit();
			System.out.println("delete role success");
			return true;
		} catch (RuntimeException e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
	}

	public static boolean delete(int id) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		try {
			Role role = session.get(Role.class, id);
			session.delete(role);
			session.getTransaction().commit();
			System.out.println("delete role success");
			return true;
		} catch (RuntimeException e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
	}

	public static List<Role> getAll() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		try {
			List<Role> list = session.createQuery("FROM Role").list();
			session.getTransaction().commit();
			return list;
		} catch (RuntimeException e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public static Role getById(int id) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		try {
			Role role = session.get(Role.class, id);
			session.getTransaction().commit();
			return role;
		} catch (RuntimeException e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public static void showAll() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		try {
			List<Role> list = session.createQuery("FROM Role").list();
			for (Role role : list)
				System.out.println(role);
		} catch (RuntimeException e) {
			session.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public static Role getByName(String name) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		try {
			List<Role> list = session.createQuery("from Role where name = :name").setParameter("name", name).list();
			if (list != null) {
				return list.get(0);
			} else {
				return null;
			}
		} catch (RuntimeException e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public static void main(String[] args) {
		Role role = getByName("ROLE_ADMIN");
		System.out.println(role);
		sessionFactory.close();
	}
}
