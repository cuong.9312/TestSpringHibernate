package cuong.test.dao;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtils {

	private static final SessionFactory sessionFactory = buildSessionFactory();

	private static SessionFactory buildSessionFactory() {
		// Tạo đối tượng cấu hình.
		Configuration configuration = new Configuration();
		// Mặc định nó sẽ đọc cấu hình trong file hibernate.cfg.xml
		// Bạn có thể chỉ định rõ file nếu muốn:
		// configuration.configure("hiberante.cfg.xml");
		configuration.configure();
		// Tạo đối tượng SessionFactory
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		return sessionFactory;
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static void shutdown() {
		// Close caches and connection pools
		// Giải phóng cache và Connection Pools.
		getSessionFactory().close();
	}
}