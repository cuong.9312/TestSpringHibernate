package cuong.test.listener;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Service;

import cuong.test.entities.User;
import cuong.test.service.UserService;

@Service("authenticationSuccessHandler")
public class SuccessAuthenticationHandler extends
		SavedRequestAwareAuthenticationSuccessHandler {

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		HttpSession session = request.getSession();
		System.out.println("login success!");
		String email = request.getParameter("email");
		User user = UserService.getByEmail(email);
		if (user != null) {
			session.setAttribute("user", user);
		}
		super.onAuthenticationSuccess(request, response, authentication);

	}

}
