package cuong.test.entities.wrapper;

import cuong.test.entities.User;

public class UserWrapper extends User {
	private static final long serialVersionUID = 1L;
	String isAdmin;

	public String getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(String isAdmin) {
		this.isAdmin = isAdmin;
	}

}
