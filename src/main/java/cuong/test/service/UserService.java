package cuong.test.service;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import cuong.test.dao.UserDAO;
import cuong.test.entities.Role;
import cuong.test.entities.User;
import cuong.test.entities.wrapper.UserWrapper;

public class UserService {
	static SessionFactory sessionFactory = new Configuration().configure()
			.buildSessionFactory();

	public static User checkLogin(String email, String password) {
		return UserDAO.checkLogin(email, password);
	}

	public static boolean insert(User user) {
		return UserDAO.insert(user);
	}

	public static boolean update(User user) {
		return UserDAO.update(user);
	}
	public static boolean updateInfo(User user) {
		return UserDAO.updateInfo(user);
	}

	public static boolean delete(User user) {
		return UserDAO.delete(user);
	}

	public static boolean delete(Integer id) {
		return UserDAO.delete(id);
	}

	public static List<User> getAll() {
		return UserDAO.getAll();
	}

	public static User getById(int id) {
		return UserDAO.getById(id);
	}

	public static User getByEmail(String email) {
		return UserDAO.getByEmail(email);
	}

	public static boolean isExist(String email) {
		return UserDAO.isExist(email);
	}

	public static void main(String[] args) {
		User user = getByEmail("cuong.9312@gmail.com");
		System.out.println(user.getName());
	}

	public static UserWrapper convert(User user) {
		UserWrapper userWrapper = new UserWrapper();
		userWrapper.setId(user.getId());
		userWrapper.setEmail(user.getEmail());
		userWrapper.setName(user.getName());
		userWrapper.setPhoneNumber(user.getPhoneNumber());
		userWrapper.setPassword(user.getPassword());
		userWrapper.setDateCreated(user.getDateCreated());
		boolean isAdmin = false;
		for (Role role : user.getRoles()) {
			if (role.getName().equalsIgnoreCase("ROLE_ADMIN"))
				isAdmin = true;
		}
		if (isAdmin) {
			userWrapper.setIsAdmin("TRUE");
		} else {
			userWrapper.setIsAdmin("FALSE");
			;
		}
		return userWrapper;
	}
}
