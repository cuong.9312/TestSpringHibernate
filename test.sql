/*
SQLyog Community v12.09 (64 bit)
MySQL - 5.6.21-log : Database - test
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`test` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `test`;

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `role` */

insert  into `role`(`id`,`name`) values (1,'ROLE_ADMIN'),(2,'ROLE_USER');

/*Table structure for table `user_roles` */

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hg_idx` (`users`),
  KEY `fk1_idx` (`role`),
  CONSTRAINT `fk1` FOREIGN KEY (`role`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk2` FOREIGN KEY (`users`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

/*Data for the table `user_roles` */

insert  into `user_roles`(`id`,`users`,`role`) values (18,17,2),(21,19,2),(22,20,2),(23,19,1),(26,23,2);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(45) NOT NULL,
  `date_created` date NOT NULL,
  `enabled` int(11) DEFAULT '1',
  `phone_number` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`email`,`password`,`name`,`date_created`,`enabled`,`phone_number`) values (17,'client@yahoo.com','$2a$10$6d20nAeygiLZLCTHa2QDGuWJ/mssRyM64vOy4KReGPI3.4XxaLsE2','client','2016-03-07',1,'01644419043'),(19,'cuong.9312@gmail.com','$2a$10$nX2VRsLwcd8hb/z5ks7a5eKcxk8rNpKE5ws4A/dd2Ze3AW00PICr.','tran huu cuong','2016-03-07',1,'01644419043'),(20,'client@gmail.com','$2a$10$KwRetupmct48YkRMJMmDN.wBOKh5YeZ3pvZ0gCrYTu1NcpqB.hzyK','guest user 1','2016-03-07',1,'01644419043'),(22,'new@tmailc.com','$2a$10$cAMxgTAky9T6mMEdU/C6duXEEmVozs/j9tkM4bETmBQbHkKJNIa1O','tran van a','2016-03-07',1,'01644419043'),(23,'ht@hdf.com','$2a$10$t0qVBD.EYe2.oUYHJMgyI.O3stI4svuqos.TYTa/7Wc.Xxx0soYEy','ádfasfasdf','2016-03-07',1,'01644419043');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
